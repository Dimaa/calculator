﻿using Calculator.API.Algebra;
using System;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main() => new Program().MainAsync().GetAwaiter().GetResult();

        private async Task MainAsync()
        {
            try
            {
                this.ExampleMatrix();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            await Task.Delay(-1);
        }

        /// <summary>
        /// Two matrices are multiplied together.
        /// </summary>
        private void ExampleMatrix()
        {
            // 3x2 Matrix
            int[,] m1 = new int[3, 2]
            {
                { 1, 0 },
                { 3, 2 },
                { 0, 4 }
            };

            // 2x4 Matrix
            int[,] m2 = new int[2, 4]
            {
                { 3, 2, 4, 0 },
                { 1, -2, 2, 1 }
            };

            var matrix1 = Matrix.Parse(m1);
            var matrix2 = Matrix.Parse(m2);

            Console.WriteLine("Matrix 1:");
            Console.WriteLine(matrix1.ToString());

            Console.WriteLine("Matrix 2:");
            Console.WriteLine(matrix2.ToString());

            Console.WriteLine("Result: ");
            Console.WriteLine(matrix1 * matrix2);
        }
    }
}
