﻿using System;
using System.Text;

namespace Calculator.API.Algebra
{
    public class Matrix
    {
        #region Variables

        private int[,] Table { get; set; }

        /// <summary>
        /// Number of rows in the matrix.
        /// </summary>
        public int Rows => this.Table.GetLength(0);

        /// <summary>
        /// Number of columns in the matrix.
        /// </summary>
        public int Columns => this.Table.GetLength(1);

        #endregion Variables

        /// <summary>
        /// Initializes a new instance of the Matrix class.
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        public Matrix(int rows, int columns)
        {
            this.Table = new int[rows, columns];
        }

        #region Operators

        public static Matrix operator +(Matrix left, Matrix right)
        {
            if ((left.Rows == right.Rows) && (left.Columns == right.Columns))
            {
                var matrix = new Matrix(left.Rows, left.Columns);

                for (int i = 0; i < left.Rows; i++)
                {
                    for (int j = 0; j < left.Columns; j++)
                    {
                        matrix.Table[i, j] = (left.Table[i, j] + right.Table[i, j]);
                    }
                }

                return matrix;
            }
            else
            {
                throw new InvalidOperationException("The matrices must be the same size");
            }
        }

        public static Matrix operator -(Matrix left, Matrix right)
        {
            if ((left.Rows == right.Rows) && (left.Columns == right.Columns))
            {
                var matrix = new Matrix(left.Rows, left.Columns);

                for (int i = 0; i < left.Rows; i++)
                {
                    for (int j = 0; j < left.Columns; j++)
                    {
                        matrix.Table[i, j] = (left.Table[i, j] - right.Table[i, j]);
                    }
                }

                return matrix;
            }
            else
            {
                throw new InvalidOperationException("The matrices must be the same size");
            }
        }

        public static Matrix operator *(Matrix left, Matrix right)
        {
            if (left.Columns == right.Rows)
            {
                var matrix = new Matrix(left.Rows, right.Columns);

                for (int i = 0; i < left.Rows; i++)
                {
                    for (int j = 0; j < right.Columns; j++)
                    {
                        int component = 0;

                        for (int k = 0; k < right.Rows; k++)
                        {
                            int component1 = left.Table[i, k];
                            int component2 = right.Table[k, j];

                            component += (component1 * component2);
                        }

                        matrix.Table[i, j] = component;
                    }
                }

                return matrix;
            }
            else
            {
                throw new InvalidOperationException("The first matrix must contain as many rows as the second matrix columns.");
            }
        }

        public static Matrix operator *(int left, Matrix right)
        {
            var matrix = new Matrix(right.Rows, right.Columns);

            for (int i = 0; i < right.Rows; i++)
            {
                for (int j = 0; j < right.Columns; j++)
                {
                    matrix.Table[i, j] = (right.Table[i, j] * left);
                }
            }

            return matrix;
        }

        public static Matrix operator *(Matrix left, int right)
        {
            return (right * left);
        }

        #endregion Operators

        /// <summary>
        /// Converts the 2D array to its matrix equivalent.
        /// </summary>
        /// <param name="a"></param>
        /// <returns>A matrix equivalent to the 2D array contained in a.</returns>
        public static Matrix Parse(int[,] a)
        {
            var matrix = new Matrix(a.GetLength(0), a.GetLength(1))
            {
                Table = a
            };

            return matrix;
        }

        /// <summary>
        /// Returns a string in the form of a table that represents the matrix.
        /// </summary>
        /// <returns>A string in the form of a table that represents the matrix.</returns>
        public override string ToString()
        {
            var matrixStringBuilder = new StringBuilder();

            for (int i = 0; i < this.Rows; i++)
            {
                var rowStringBuilder = new StringBuilder();

                for (int j = 0; j < this.Columns; j++)
                {
                    rowStringBuilder.Append($"{this.Table[i, j].ToString("00")} ");
                }

                matrixStringBuilder.AppendLine(rowStringBuilder.ToString());
            }

            return matrixStringBuilder.ToString();
        }
    }
}
